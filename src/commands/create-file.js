
module.exports = {
  name: 'create-file',
  alias: ['c'],
  description: `Cria um arquivo do tipo especificado nas opções.\nclerk [create-file;c] [sql;js] ['nome do arquivo']\n\nParametros opcionais:\n[-n 'nome do arquivo'; --nome='nome do arquivo'; --name='nome do arquivo']\n[-u 'nome do autor'; --nomeusuario='nome do autor'; --username='nome do autor'] \n[--sf='subdiretorio';--subfolder='subdiretorio onde arquivo será gerado']\n`,
  run: async toolbox => {
    const {
      parameters,
      template: { generate },
      print: { error, info, success },
      CreateFile
    } = toolbox;
      
    //console.log(parameters);
    function getFileName(){
      if (parameters.second !== undefined)
        return parameters.second;
      else if(parameters.options.n !== undefined)
        return parameters.options.n;
      else if (parameters.options.name !== undefined)
        return parameters.options.name;
      else if (parameters.options.nome !== undefined)
        return parameters.options.nome;
    
      return undefined;
    };
    function getUserName(){
      if (parameters.options.u !== undefined)
        return parameters.options.u;
      else if (parameters.options.username !== undefined)
        return parameters.options.username;
      else if (parameters.options.nomeusuario !== undefined)
        return parameters.options.nomeusuario;
      return undefined;
    };
    function getSubFolder(){
      if (parameters.options.subfolder !== undefined)
      { 
        if (parameters.options.subfolder !== true)
          return parameters.options.subfolder;
        else
          return 'script'; 
      }
      else if (parameters.options.sf !== undefined)
      { 
        if (parameters.options.sf !== true)
          return parameters.options.sf;
        else
          return 'script';          
      }
      else
        return undefined;
    };
    const subfolder = getSubFolder();
    const fileName = getFileName();
    const userName = getUserName();
    const typeFile = parameters.first;
    CreateFile(userName, fileName, typeFile, subfolder);

  },
};