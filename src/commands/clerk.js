
const command = {
  name: 'clerk',
  description:'COMANDOS [opções]',
  run: async toolbox => {
    const { print, parameters } = toolbox

    //print.error("I can't understand what you wrote..."); 
    print.error("Desculpa, eu não entendi o que escreveu...\n");
    print.info("Utilize o comando 'clerk -h' para ver as opções possíveis");

  }
}

module.exports = command
