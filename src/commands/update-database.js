
module.exports = {
  name: 'update-database',
  description:`Aglutina todos os scripts sql em um único arquivo e executa o arquivo gerado\nno banco selecionado.\nclerk [update-database;updb] ['diretorio']`,
    alias: ['updb'],
    run: async toolbox => {
      const {
        parameters,
        template: { generate },
        print: { error, info, success }, 
        filesystem,
        CreateFileNoIdentifier,
        NovaConexao
      } = toolbox;
      const upFileName = 'update-database';

      function getDir()
      {
        if (parameters.second !== undefined)
          return parameters.second;
        else if (parameters.options.d !== undefined)
          return parameters.options.d;
        else if (parameters.options.dir !== undefined)
          return parameters.options.dir;
        else if (parameters.options.directory !== undefined)
          return parameters.options.directory;
        return '';
      }

      const dirExc = filesystem.cwd(process.cwd());
      const files = dirExc.find(getDir(),{ matching: '*.sql' });
      const iniFile = `
      DO 
      $UP$
      BEGIN
      `;
      const endFile = `
      END;
      $UP$`;
     
      async function CreateFileDatabase(list = [])
      {
        var temp=iniFile, newScript = filesystem;
        

        if (list.length > 0)
          for (i = 0; i < list.length; i++)
          {
            temp += (filesystem.read(dirExc.cwd() + '/' + list[i]));
            /*temp += `
            DO
            $FILE$
            BEGIN
            ` + (filesystem.read(dirExc.cwd() + '/' + list[i])) + `
            EXCEPTION 
            WHEN OTHERS THEN
              RAISE EXCEPTION 'Erro no arquivo ${list[i]}: %', SQLERRM;
            END;
            $FILE$
            `;*/
          }
        else
        {
          //error("I can't find your scripts files");
          error("Não consigo encontrar os scripts...");
          return;
        }
        temp += endFile;

        newScript.write(dirExc.cwd() + '/update-database.sql', temp.toString());
      }
      async function UpDataBase()
      {
        let script = (filesystem.read(dirExc.cwd() + '/' + upFileName + '.sql'));
        let db = NovaConexao()
        db
          .connect()
          .then(() => success('Conexão feita!'))
          //.then(()=>info(files))
          .then(() => db.query(script))
            .catch(erro => error('Erro ao tentar conectar com o banco de dados.' + erro))
            .finally(() => db.end())
      }

      await CreateFileDatabase(files);
      UpDataBase();
      success('Eu estou upando o banco');
    }
  }
  