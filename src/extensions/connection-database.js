module.exports = (toolbox) =>{    
    const { Client } = require('pg');

    function novaConexao(userName = 'postgres', pass = 'postgres', hostName = 'localhost', portNumber = '5432', dataBase = 'postgres', sslConfig = '') {
        clientBancoDeDados = new Client({
            user:userName,
            password: pass,
            host: hostName,
            port: portNumber,
            database: dataBase,
            ssl: sslConfig
        })
        return clientBancoDeDados
    }

    toolbox.NovaConexao = novaConexao;

};
  