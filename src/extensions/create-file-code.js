module.exports = (toolbox) =>{
    const { filesystem, print: { success, error }, template } = toolbox;
      
        //console.log(parameters);
    var date = new Date(Date.now());
    
    async function getStringDateNow() {
        let data = {
            hora: date.getHours().toString(),
            minutos: date.getMinutes().toString(),
            segundo: date.getSeconds().toString(),
            dia: date.getDate().toString(),
            mes: (date.getMonth() + 1).toString(),
            ano: date.getFullYear().toString()
        };
        return (data.dia + data.mes + data.ano + data.hora + data.minutos + data.segundo);
    };
    async function selectTempleteFile(type)
    {
        switch (type)
        {
            case ('sql'):
                return 'sql-model.js.ejs';
            case ('js'):
                return 'js-model.js.ejs';
            default:
                error("The file type isn't support");
                return;

        }
    }
    async function createFile(uname = '', name = undefined, typefile, subfolder = undefined, contentFile = '--YOUR DATABASE SCRIPT')
    {   
        const codigoUni = await getStringDateNow();

        if (name === undefined) {
            error('The file name must be specified');
            return;
        }

       
        if (typefile === undefined)
        {
            error('The file type must be specified');
            return;
        }
        
        /*if (subfolder !== undefined) {
            error('The subfolder name must be specified');
            return;
        }*/
        await template.generate({
            template: await selectTempleteFile(typefile),
            target: `${(subfolder !== undefined) ? (subfolder + '/') : ('')}${codigoUni} - ${name}.${typefile}`,
            props: {
                name,
                userName: uname,
                date,
                content: contentFile
            }
        });

        success(`Create ${codigoUni} - ${name}.${typefile} in './${(subfolder !== undefined) ? (subfolder + '/') : ('')}'`);
    };
    async function createFileNoIdentifier(uname = '', name = undefined, typefile, subfolder = undefined, contentFile = '--YOUR DATABASE SCRIPT')
    {   
      
        if (name === undefined) {
            error('The file name must be specified');
            return;
        }

       
        if (typefile === undefined)
        {
            error('The file type must be specified');
            return;
        }
        
        if (subfolder !== undefined) {
            error('The subfolder name must be specified');
            return;
        }
        await template.generate({
            template: await selectTempleteFile(typefile),
            target: `${(subfolder !== undefined) ? (subfolder + '/') : ('')}${name}.${typefile}`,
            props: {
                name,
                userName: uname,
                date,
                content: contentFile
            }
        });

        success(`Create ${name}.${typefile} in '${('./'+subfolder === './') ? ('./'+subfolder + '/') : (process.cwd())}'`);
    };
    toolbox.CreateFile = createFile;
    toolbox.CreateFileNoIdentifier = createFileNoIdentifier

};
  